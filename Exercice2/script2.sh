#!/bin/bash

# ########## #
# Exercice 2 #
# ########## #

# Partie A

ls *.php
ls *.log
ls ?i*
ls [b-s]*
ls [!aeiouy]*
ls *.!(php)
ls *.!(php|txt)
ls *@(866)*
ls *
ls *@(abc|866)*

# Partie B

cd /tmp && ls -l

echo """Les commandes dans le shell sont:
                                Commandes internes
                                Commandes externes
     """
    

who -A 2> error.txt
who -A 2> /dev/null

echo "f1:f2:f3:f4" | cut -d: -f1,3

cat /etc/passwd | file ## file n'es pas une commande

