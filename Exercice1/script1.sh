#!/bin/bash

# ########## #
# Exercice 1 #
# ########## #

# Partie 1

mkdir Formation
cd Formation
mkdir Unix Python
mkdir Unix/TP1 Unix/TP2
mkdir Python/TP1 Python/TP2
mkdir "Unix/TP1/Seance 1" "Unix/TP1/Seance 2"
touch "Unix/TP1/Seance 1/exo1" "Unix/TP1/Seance 1/exo2"
cp "Unix/TP1/Seance 1/exo"{1..2} Python/TP2/

# Partie 2

cd Python/TP1
pwd
cd ../../Unix/TP2
cd ~/programmation-shell/Exercice1/Formation/Python/TP1
cd ~
cd ~/programmation-shell/Exercice1/Formation
rmdir Python/TP2
mv Python/TP2/exo1 Python/TP1/
rmdir -r "Unix/TP1/Seance "{1..2}
mv Python/TP1/exo1 Unix/TP1
rmdir -r "Unix/TP1/Seance "{1..2}
mv Python/TP2/exo2 Python/TP2/exercice2
ln -s Python/TP2/exercise2 exo2
ln Python/TP2/exercice2 exo_2
ls -li ./ Python/TP2/
