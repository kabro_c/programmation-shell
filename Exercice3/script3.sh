#!/bin/bash

# ########## #
# Exercice 3 #
# ########## #

env
message="Bonjour la classe"
export message
bash
echo $message

export PATH="$PATH:/home/kabro/programmation-shell/Exercice1/Formation/"

export PS1="\[\033[38;5;11m\]\u\[\]\[\033[38;5;6m\][\w]\[\]: \[\]"
export PS2="\$\[\]\[\033[38;5;11m\]>\[\]"

alias bonjour='echo "Bonjour tout le monde"'
alias pinglocal='ping 127.0.0.1'
alias date='echo "La date système est caché dans ce shell"'

set HISTSIZE=5
set +o HISTSIZE

alias

alias p='ps -ef | more'
unalias p

alias p='ps -ef | more' >> ~/.bashrc

# 12 : le PS2 qui attent la suite de la commande ,
# CTR-C annule le process et permet de sortir de ce cas.
