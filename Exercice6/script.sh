#!/bin/bash

# ########## #
# Exercice 6 #
# ########## #

function sleep
{
    echo "pleas press enter to return to the menu"
    read i
}

function addition
{
    sum=0

    for l in $(cat $1)
    do
      sum=$(( sum + l ))  
    done
    echo "Sum of number in file is : ${sum}"
}

function nlignes
{
    res=$(cat $1 | wc -l)
    echo "Number of line in file is : ${res}"
}

function minmax
{
    max=0
    min=100000

    for l in $(cat $1)
    do
        if (( $max < $l ))
        then
            max=$l
        fi

        if (( $min > $l ))
        then
            min=$l
        fi
    done
    echo "min is : ${min} and max is : ${max}"
}


if [[ -f "$1" ]]
then
    while true
    do
        clear
        echo "- 1 - Addition"  
        echo "- 2 - nlignes"
        echo "- 3 - minmax"
        echo "- 0 - Fin"  
        echo -e "Votre choix : \c" 
        read option

        case $option in
        0)
            exit 0
            ;;
        1)
            addition $1
            ;;
        2)
            nlignes $1
            ;;
        3)
            minmax $1
            ;;
        *)
            echo "Bad Option"
            ;;
        esac
            sleep
    done
else
    echo "file not found"
    exit 1
fi
