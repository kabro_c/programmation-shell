#!/bin/bash

# ########## #
# Exercice 6 #
# ########## #

function sleep
{
    echo "pleas press enter to return to the menu"
    read i
}

function users_connect
{
    who
}

function disk_space
{
    df -k
}



while true
do
    clear
    echo "- 1 - Afficher la liste des utilisateurs connectes"  
    echo "- 2 - Afficher l’espace disque"
    echo "- 0 - Fin"  
    echo -e "Votre choix : \c" 
    read option

    case $option in
    0)
        exit 0
        ;;
    1)
        users_connect
        ;;
    2)
        disk_space
        ;;
    *)
        echo "Bad Option"
        ;;
    esac
        sleep
done
