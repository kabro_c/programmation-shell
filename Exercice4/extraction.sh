#!/bin/bash

# ########## #
# Exercice 4 #
# ########## #

# Partie D

ch1=$1
read ch2

if (( ${#ch1} < ${#ch2} ))
then
	ch2=$(echo $ch2 | cut -c$((${#ch1}+1))-)
	echo $ch2
else
	echo "Extraction impossible !!"
fi
