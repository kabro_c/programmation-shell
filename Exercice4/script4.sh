#!/bin/bash

# ########## #
# Exercice 4 #
# ########## #

# Partie A

prenom="Charbel"
echo $prenom

nom_et_prenom="Charbel Kabro"
echo $nom_et_prenom

unset prenom
unset nom_et_prenom

prenom="Charbel"
nom="Kabro"
echo "${prenom} ${nom}"

# Partie B

echo "Nous sommes le $(date '+%a %B %d %T %Z %Y %u')."
echo "Nous sommes le $(date '+%D')."

user=$(tail -1 /etc/passwd | cut -d: -f1)

nbuser=$(cat /etc/passwd | wc -l)

# Partie C

echo *      # f1 f2 f3
echo \*     # *
echo "*"    # *
echo ’*’    # '*'
age=20      # assigne 20 a la variable age
echo $age   # 20
echo ${age} # 20
echo \$age  # $age
echo "$age" # 20
echo ’$age’ # '20’
echo "Tu es $(logname) et tu as -> $age ans" # Tu es kabro et tu as -> 20 ans
echo Tu es $(logname) et tu as -> $age ans  
# crée le fichier 20(la variable age avec ce que l'echo affiche)
