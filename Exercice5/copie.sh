#!/bin/bash

# ########## #
# Exercice 5 #
# ########## #

copy=$1
dest=$2

if [[ -f "$1" ]]
then
    if [[ -f "$2" ]]
    then
        echo "Destination file already exist"
    else
        cp $1 $2
    fi
else
    echo "Copy file not found"
fi
