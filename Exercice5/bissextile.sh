#!/bin/bash

# ########## #
# Exercice 5 #
# ########## #

if (( $1 % 4 ))
then
	echo "${1} are not Bisextil"
else
	echo "${1} are Bisextil"
fi

