#!/bin/bash

# ########## #
# Exercice 5 #
# ########## #

fact=1
i=1

while (( $i <= $1 ))
do
   fact=$(( fact * $i ))
   i=$(( i + 1 ))
done
echo $fact
