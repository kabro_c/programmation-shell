#!/bin/bash

# ########## #
# Exercice 5 #
# ########## #

i=1
while (( $i <= 12 ))
do
    echo "Le mois $(date --date="00-${i}-01" +%B) est le mois numéro ${i}"
    i=$(( i + 1 ))
done
