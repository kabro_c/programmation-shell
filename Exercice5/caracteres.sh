#!/bin/bash

# ########## #
# Exercice 5 #
# ########## #

i=0
type=""

while (( $i < ${#1}))
do
    l=$(echo ${1:i:1})
    odd=$(echo ${l} | tr -d "\n" | od -An -t dC)

    if (( $odd >= 65)) && (( $odd <= 90))
    then
        type="Upper char"
    elif (( $odd >= 97)) && (( $odd <= 122))
    then
        type="Tiny char"
    elif (( $odd >= 48)) && (( $odd <= 57))
    then
        type="Number char"
    else
        type="Spetial char"
    fi
    echo "la lettre \"${l}\" corespond au type : ${type}"
    
    i=$(( i + 1 ))
done
