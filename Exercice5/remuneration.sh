#!/bin/bash

# ########## #
# Exercice 5 #
# ########## #

if (($1 > 40))
then
    sup=$(expr ${1} - 40)
    salaire=$(expr ${sup} \* 12)
    echo "Vous avez ${sup} heure sup donc ${salaire} euro"
else
    echo "Pas d'heure sup pas de salaire sup !"
fi
