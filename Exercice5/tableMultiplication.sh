#!/bin/bash

# ########## #
# Exercice 5 #
# ########## #

i=0
 while (( $i <= 10 ))
 do
    echo "${1} * ${i} = $(expr $1 \* $i)"
    i=$(( i + 1 ))
done
