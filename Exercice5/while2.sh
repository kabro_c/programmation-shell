#!/bin/bash

# ########## #
# Exercice 5 #
# ########## #

i=1
while (($#))
do
    echo "L’argument ${i} est : ${1}"
    shift
    i=$(( i + 1 ))
done
