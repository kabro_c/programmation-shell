#!/bin/bash

# ########## #
# Exercice 5 #
# ########## #

i=0
while (( $i <= $1 ))
do
   if (( $(($i % 2)) == 0 ))
   then
        echo $i
   fi
   i=$(( i + 1 ))
done
