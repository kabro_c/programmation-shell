#!/bin/bash

# ########## #
# Exercice 5 #
# ########## #

tab=(100 4 548 6 70 8 9 10)
max=0
min=${tab[0]}
i=0

while (( $i < ${#tab[@]} ))
do
    if (( $max < ${tab[i]} ))
    then
        max=${tab[i]}
    fi

    if (( $min > ${tab[i]} ))
    then
        min=${tab[i]}
    fi
    i=$(( i + 1 ))
done

echo "min is : ${min} and max is : ${max}"
